//
//  ViewController.swift
//  calculadoraIMC
//
//  Created by Student on 13/09/22.
//  Copyright © 2022 Student. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var peso: UITextField!
    @IBOutlet weak var altura: UITextField!
    @IBOutlet weak var resultado: UILabel!
    
    @IBAction func calcularIMC(_ sender: Any) {
        let pesoFloat = Float(peso.text!) ?? 0
        let alturaFloat = Float(altura.text!) ?? 0
        
        if (pesoFloat == 0 && alturaFloat == 0) {
            resultado.text = "Valores inválidos"
            peso.text = ""
            altura.text = ""
            return
        }
        
        let imc = pesoFloat / (alturaFloat * alturaFloat)
        
        resultado.text = String(format: "%.2f", imc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

